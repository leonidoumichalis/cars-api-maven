
# Build stage

FROM maven:3.8.6-jdk-11
COPY src /src
COPY pom.xml /cars-api-maven
RUN mvn -f /pom.xml clean package



# Package stage
FROM openjdk:11
COPY --from=build /target/cars-api-maven-0.0.1-SNAPSHOT.jar cars-api-maven-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","cars-api-maven-0.0.1-SNAPSHOT.jar"]
